FROM almalinux:latest
COPY requirements.txt .
COPY requirements.yml .
RUN yum install -y pip sshpass openssh-clients unzip wget git rsync nc && \
	pip install -r requirements.txt --no-cache-dir && \
	ansible-galaxy install -r requirements.yml -p /usr/share/ansible/collections && \
	yum -y clean all  && \
	rm -rf /var/cache
COPY pip-installer.sh .
RUN ./pip-installer.sh